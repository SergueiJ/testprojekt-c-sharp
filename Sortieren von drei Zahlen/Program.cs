﻿using System;

namespace Sortieren_drei_Zahlen
{
    class Program
    {

        int ersteZahl = 0;
        int zweiteZahl = 0;
        int dritteZahl = 0;
        static void Main(string[] args)
        {
            Program Sortieren_drei_Zahlen = new Program();
            Sortieren_drei_Zahlen.eva();

        }

        public void eva()
        {

            eingabe(); //Eingabe von drei Zahlen
            verarbeitung(); //Sortieren von drei Zahlen + Ausgabe der sortierten Zahlen
            ausgabe(); //Text: "Dies sind die sortierten Zahlen"


        }

        private void eingabe()
        {
            Console.WriteLine("Bitte geben Sie die erste Zahl ein:");
            ersteZahl = int.Parse(Console.ReadLine());
            Console.WriteLine("Bitte geben Sie die zweite Zahl ein:");
            zweiteZahl = int.Parse(Console.ReadLine());
            Console.WriteLine("Bitte geben Sie die dritte Zahl ein:");
            dritteZahl = int.Parse(Console.ReadLine());
        }

        private void verarbeitung()
        {
            if(ersteZahl < zweiteZahl)
            {
                if(ersteZahl < dritteZahl)
                {
                    if(zweiteZahl < dritteZahl)
                    {
                        Console.WriteLine("Ausgabe: " + ersteZahl + ", " + zweiteZahl + ", " + dritteZahl);
                    }
                    else
                    {
                        Console.WriteLine("Ausgabe: " + ersteZahl + ", " + dritteZahl + ", " + zweiteZahl);
                    }

                }
                else
                {
                    Console.WriteLine("Ausgabe: " + dritteZahl + ", " + ersteZahl + ", " + zweiteZahl);
                }

            }
            else
            {
                if(zweiteZahl < dritteZahl)
                {
                    if(ersteZahl < dritteZahl)
                    {
                        Console.WriteLine("Ausgabe: " + zweiteZahl + ", " + ersteZahl + "," + dritteZahl);
                    }
                    else
                    {
                        Console.WriteLine("Ausgabe: " + zweiteZahl + ", " + dritteZahl + ", " + ersteZahl);
                    }
                }
                else
                {
                    Console.WriteLine("Ausgabe: " + dritteZahl + ", " + zweiteZahl + ", " + ersteZahl);
                }
            }
        }

        private void ausgabe()
        {
            Console.WriteLine("Dies sind die sortierten Zahlen.");
        }
    }   

}
