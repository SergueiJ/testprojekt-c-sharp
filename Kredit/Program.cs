﻿using System;

namespace Kredit
{
    class Program
    {
        double Kreditbetrag = 0;
        double Rate = 0;
        double ProzentRate = 0;
        double Zinsen = 0; //Tilgung + Zinsprozent (in Prozent)
        double Tilgung = 0; //Prozentsatz der Tilgung
        double Zinsprozent = 0; //Prozentsatz des Sollzins
        double KontostandSoll = 0;
        //double Endbetrag = 0;

        static void Main(string[] args)
        {
            var program = new Program();
            var Kredit = program;
            Kredit.eva();

        }


        public void eva()
        {
            eingabe();
            verarbeitung();
            ausgabe();
        }

        public void eingabe()
        {
            Console.WriteLine("Willkommen in der CSharpBank!");
            Console.WriteLine("Bitte geben Sie den gewünschten Kreditbetrag an:");
            Kreditbetrag = double.Parse(Console.ReadLine());
            Console.WriteLine("Bitte geben Sie die gewünschte Prozenthöhe (%) der Tilgung an:");
            Tilgung = double.Parse(Console.ReadLine());
            Console.WriteLine("Bitte geben Sie die gewünschte Prozenthöhe (%) des Sollzins an:");
            Zinsprozent = double.Parse(Console.ReadLine());

        }


        public void verarbeitung()
        {
            ProzentRate = Math.Round((Tilgung + Zinsprozent), 2);
            Zinsen = Math.Round(((Zinsprozent / 100) * Kreditbetrag) / 12, 2);
            Rate = Math.Round(((ProzentRate / 100) * Kreditbetrag) / 12, 2);
            //KontostandSoll = Math.Round((Kreditbetrag - Rate) + Zinsen);
            //Endbetrag == 0;
            do
            {
                KontostandSoll = Kreditbetrag - Rate;
                KontostandSoll = KontostandSoll + Zinsen;
                KontostandSoll = Math.Round((Kreditbetrag - Rate) + Zinsen);

            }
            while (KontostandSoll != null);

        }

        public void ausgabe()
        {
            Console.WriteLine("Kontostand soll " + KontostandSoll + " EUR");
            Console.WriteLine("Ihre monatlichen Zinsen betragen " + Zinsen + " EUR");
            Console.WriteLine("Ihr Rate in Prozenten beträgt: " + ProzentRate + " %");
            Console.WriteLine("Ihre monatliche Rate beträgt " + Rate + " EUR");

        }
    }     
}
